#pragma  once
#include <mutex>
#include <shared_mutex>

#include <Container/Blackboard.hpp>

namespace TD_SAFE_BB {
	template<typename T, typename BBS>
	class BBObject {
	public:
		BBObject(BBS& bb, const Container::BlackboardID id, T val) :_bb(bb), _id(id) {
			_bb.declare<T>(_id, val);
		}

		void SetVal(T val) {
			std::unique_lock<std::shared_mutex> lock(_mutex);
			_bb.setValue<T>(_id, val);
		}

		T GetVal() {
			std::shared_lock<std::shared_mutex> lock(_mutex);
			return _bb.getValue<T>(_id);
		}

	protected:
		BBS& _bb;
		const Container::BlackboardID _id;
		mutable std::shared_mutex _mutex;
	};

}