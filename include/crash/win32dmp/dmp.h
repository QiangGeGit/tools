#pragma once
#include <DbgHelp.h>
#pragma comment(lib, "dbghelp.lib")

namespace DMP {
	static MINIDUMP_TYPE _mt = MiniDumpWithFullMemory;

	static LONG WINAPI exception_handler(struct _EXCEPTION_POINTERS* excp_pointer)
	{
		TCHAR ext_name[] = _T(".dmp");
		TCHAR file_name[0x400];
		HANDLE	hf;
		DWORD	sz_fname;

		sz_fname = _countof(file_name) - (_countof(ext_name) + 1);
		if (GetModuleFileName(NULL, file_name, sz_fname)) {
			_tcscat_s(file_name, _countof(file_name), ext_name);
			hf = CreateFile(file_name, GENERIC_WRITE, FILE_SHARE_WRITE, NULL, CREATE_ALWAYS, FILE_ATTRIBUTE_NORMAL, NULL);
			if (hf != INVALID_HANDLE_VALUE) {
				MINIDUMP_EXCEPTION_INFORMATION   excp_info;
				excp_info.ThreadId = GetCurrentThreadId();
				excp_info.ExceptionPointers = excp_pointer;
				excp_info.ClientPointers = FALSE;
				MiniDumpWriteDump(
					GetCurrentProcess(),
					GetCurrentProcessId(),
					hf,
					_mt,
					&excp_info,
					NULL,
					NULL);
				CloseHandle(hf);
			}
		}

		ExitProcess(0);

		return EXCEPTION_EXECUTE_HANDLER;
	}

	static void SetDmp(MINIDUMP_TYPE mt = MiniDumpWithFullMemory) {
		_mt = mt;
		::SetUnhandledExceptionFilter(exception_handler);
	}

}