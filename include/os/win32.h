#pragma once

#include <windows.h>
#include <tchar.h>
#include <stdio.h>
#include <psapi.h>

#include <set>
#include <string>
#include <vector>

namespace OS {
	namespace PROCINFO {

		static std::set<DWORD> pids() {
			std::set<DWORD> pids;

			do
			{
				DWORD aProcesses[1024];
				DWORD cbNeeded;
				if (!EnumProcesses(aProcesses, sizeof(aProcesses), &cbNeeded))
					break;

				DWORD cProcesses = cbNeeded / sizeof(DWORD);

				for (unsigned int i = 0; i < cProcesses; i++) {
					pids.insert(aProcesses[i]);
				}
			} while (false);

			return std::move(pids);
		}

		std::vector<std::wstring> pidModules(const DWORD dwpid) {
			HMODULE hMods[1024];
			HANDLE hProcess;
			DWORD cbNeeded;
			unsigned int i;

			wchar_t buf[1024] = { 0 };
			std::vector<std::wstring> mods;

			do
			{
				if (_ltow_s(dwpid, buf, 10)) {
					break;
				}
				hProcess = OpenProcess(PROCESS_QUERY_INFORMATION |
					PROCESS_VM_READ,
					FALSE, dwpid);
				if (NULL == hProcess)
					break;

				if (EnumProcessModules(hProcess, hMods, sizeof(hMods), &cbNeeded))
				{
					for (i = 0; i < (cbNeeded / sizeof(HMODULE)); i++)
					{
						TCHAR szModName[MAX_PATH];
						if (GetModuleFileNameEx(hProcess, hMods[i], szModName,
							sizeof(szModName) / sizeof(TCHAR)))
						{
							wchar_t info[MAX_PATH * 2] = { 0 };
							swprintf_s(info, L"[0x%08X]\t%s", (unsigned)hMods[i], szModName);
							mods.push_back(info);
						}
					}
				}

				CloseHandle(hProcess);
			} while (false);

			return std::move(mods);
		}

	}

	namespace PROCESS {

	}
}
