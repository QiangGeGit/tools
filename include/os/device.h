#pragma once
#include <SetupAPI.h>
#pragma comment(lib,"Setupapi.lib")

#include <cfgmgr32.h>
#include <map>

#define RET_ON_NOTEMPTY(X) \
if (!X.empty()) {\
return X;\
}\

namespace QOS {
	std::string StrGuid(const GUID& guid) {
		// {A508949E-64D5-49D5-8CF2-C9B4D94ADCDD}
		char strGuid[] = "{A508949E-64D5-49D5-8CF2-C9B4D94ADCDD}";
		sprintf_s(strGuid,sizeof(strGuid), "{%08X-%04X-%04X-%02X%02X-%02X%02X%02X%02X%02X%02X}", 
			guid.Data1, guid.Data2, guid.Data3, 
			guid.Data4[0],
			guid.Data4[1],
			guid.Data4[2],
			guid.Data4[3],
			guid.Data4[4],
			guid.Data4[5],
			guid.Data4[6],
			guid.Data4[7]
		);
		return strGuid;
	}

	namespace QDEVICE {
		class CDevInstance;

		typedef std::vector<CDevInstance> VecDevInst;
		typedef VecDevInst::iterator ItorDevInst;

		struct cmpGUID {
			bool operator () (const GUID& l,const  GUID& r) const {
				return StrGuid(l) > StrGuid(r);
			}
		};

		typedef std::map<GUID, std::wstring,cmpGUID> mapInterface;
	
		class CDevInstance {
		public:
			CDevInstance(const SP_DEVINFO_DATA& sdd)
				:m_sdd(sdd)
			{
			}
			~CDevInstance() {
			}

			operator bool() const{
				return !!m_sdd.cbSize;
			}

			const GUID guidClass() const {
				return m_sdd.ClassGuid;
			}

			const std::wstring& InstID(const HDEVINFO& set) {
				RET_ON_NOTEMPTY(m_instID)

				DWORD dwSz = 0;
				SetupDiGetDeviceInstanceIdW(set, &m_sdd, NULL, 0, &dwSz);
				if (GetLastError() == ERROR_INSUFFICIENT_BUFFER) {
					dwSz++;
					size_t chs = dwSz;
					wchar_t* id = NULL;
					dwSz *= sizeof(*id);
					id = (wchar_t*)malloc(dwSz);
					if (id) {
						ZeroMemory(id, dwSz);
						dwSz = 0;
						SetupDiGetDeviceInstanceIdW(set, &m_sdd, id, chs, &dwSz);
						m_instID = id;
						free(id);
					}
				}
				return m_instID;
			}

			const std::wstring& ParentIstID(const HDEVINFO& set) {
				RET_ON_NOTEMPTY(m_ParentInstID)

				DEVINST parentDevInst;
				if (CM_Get_Parent(&parentDevInst, m_sdd.DevInst, 0) != CR_SUCCESS) {
					return m_ParentInstID;
				}

				wchar_t parentDevInstId[512] = { 0 };
				if (CM_Get_Device_IDW(parentDevInst, parentDevInstId, sizeof(parentDevInstId), 0) == CR_SUCCESS) {
					m_ParentInstID = parentDevInstId;
				}

				return m_ParentInstID;
			}

			const mapInterface& InstPath(const HDEVINFO& set,const GUID* interfaceClassGuid) {
				RET_ON_NOTEMPTY(m_Interface)

				if (*this) {
					SP_DEVICE_INTERFACE_DATA sdid = { sizeof(SP_DEVICE_INTERFACE_DATA) };
					DWORD dwIndex = 0;
					while (SetupDiEnumDeviceInterfaces(set,&m_sdd, interfaceClassGuid, dwIndex++, &sdid)) {
						DWORD dwSz = 0;
						SetupDiGetDeviceInterfaceDetailW(
							set,
							&sdid,
							NULL,
							0,
							&dwSz,
							NULL);
						if (GetLastError() == ERROR_INSUFFICIENT_BUFFER) {
							PSP_DEVICE_INTERFACE_DETAIL_DATA_W psdidd = (PSP_DEVICE_INTERFACE_DETAIL_DATA_W)malloc(dwSz);
							if (psdidd) {
								ZeroMemory(psdidd, dwSz);
								psdidd->cbSize = sizeof(*psdidd);
								if (SetupDiGetDeviceInterfaceDetailW(set, &sdid, psdidd, dwSz, NULL, NULL)) {
									m_Interface[sdid.InterfaceClassGuid] = psdidd->DevicePath;
								}
								free(psdidd);
							}
						}
					}
				}

				return std::move(m_Interface);
			}

		private:

		private:
			SP_DEVINFO_DATA m_sdd;

			std::wstring m_instID;
			std::wstring m_ParentInstID;

			mapInterface m_Interface;
		};

		class CDeviceSet {
		public:
			CDeviceSet(const GUID* pCls = NULL, const wchar_t* pEnumerator = NULL, const HWND hParent = NULL, const DWORD dwFlag = DIGCF_ALLCLASSES)
				:m_hdevInfo(INVALID_HANDLE_VALUE)
				, m_pCls(pCls)
				, m_enumerator(pEnumerator)
				, m_hWndParent(hParent)
				, m_flags(dwFlag)
			{
				Init();
			}
			~CDeviceSet() { UnInit(); }

			operator bool() const{
				return m_hdevInfo != INVALID_HANDLE_VALUE;
			}

			operator const HDEVINFO() const {
				return m_hdevInfo;
			}

			const VecDevInst DevInsts() {
				VecDevInst vec;
				if (*this) {
					SP_DEVINFO_DATA sdd = { sizeof(SP_DEVINFO_DATA) };
					DWORD dwIndex = 0;
					while (SetupDiEnumDeviceInfo(*this, dwIndex++, &sdd)) {
						vec.push_back(CDevInstance(sdd));
					}
				}

				return std::move(vec);
			}

		private:
			void Init() {
				m_hdevInfo = SetupDiGetClassDevsW(m_pCls, m_enumerator, m_hWndParent, m_flags);
			}
			void UnInit() {
				if (m_hdevInfo) {
					SetupDiDestroyDeviceInfoList(m_hdevInfo);
				}
			}

		private:
			HDEVINFO m_hdevInfo;

			const GUID* m_pCls;
			const wchar_t* m_enumerator;
			const HWND m_hWndParent;
			const DWORD m_flags;
		};
	}
}