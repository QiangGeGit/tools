#pragma once

#include <vector>
#include <array>
#include <string>
#include <thread>
#include <atomic>
#include <time/time.hpp>

#include <WS2tcpip.h>
#include <winsock.h>

#define USE_CONSOLE
#ifdef _DEBUG
#include <log/log.h>
#endif
#include <map>

#define DEF_NET_TIMEOUT (5000)

static std::atomic<time_t> tmntc = 0;

static const char* ntcHosts[] = {
	"pool.ntp.org",
	"cn.pool.ntp.org",
	"0.cn.pool.ntp.org",
	"1.cn.pool.ntp.org",
	"2.cn.pool.ntp.org",
	"3.cn.pool.ntp.org",
	"time.windows.com",
	"us.pool.ntp.org",
	"uk.pool.ntp.org",

	//"tw.pool.ntp.org",
	//"0.tw.pool.ntp.org",
	//"1.tw.pool.ntp.org",
	//"2.tw.pool.ntp.org",
	//"3.tw.pool.ntp.org",
	//"time.nist.gov",
	//"time-nw.nist.gov",
	//"asia.pool.ntp.org",
	//"europe.pool.ntp.org",
	//"oceania.pool.ntp.org",
	//"north-america.pool.ntp.org",
	//"south-america.pool.ntp.org",
	//"africa.pool.ntp.org",
	//"ca.pool.ntp.org",
	//"au.pool.ntp.org",
};

size_t dns_lookup(const char* host, PCSTR servicename, std::vector<sockaddr_in*>& vecAddr) {
	static std::map<std::string, std::vector<sockaddr_in*>> hostIps;
	std::map<std::string, std::vector<sockaddr_in*>>::iterator hostip = hostIps.find(host);
	if (hostip != hostIps.end()) {
		vecAddr = hostip->second;
		return vecAddr.size();
	}

	struct addrinfo* result;
	int ret = getaddrinfo(host, servicename, NULL, &result);
	for (struct addrinfo* p = result; p; p = p->ai_next)
	{
		if (p->ai_family != AF_INET) {
			continue;
		}

		struct sockaddr_in* addr = (sockaddr_in*)malloc(sizeof(sockaddr_in));
		if (!addr) {
			continue;
		}
		memcpy(addr, p->ai_addr, sizeof(*addr));
		vecAddr.push_back(addr);
	}
	freeaddrinfo(result);

	hostIps[host] = vecAddr;
	return vecAddr.size();
}

void NTCHostTime(const char* host, size_t tmout = DEF_NET_TIMEOUT, WSAEVENT wsaEvnt = NULL) {
	std::vector<sockaddr_in*> vecAddr;
	dns_lookup(host, "ntp", vecAddr); /* Helper function defined below. */

	auto f = [=](sockaddr_in* addr)->void {
		NTPMessage msg;
		/* Important, if you don't set the version/mode, the server will ignore you. */
		msg.clear();
		msg.version = 3;
		msg.mode = 3 /* client */;

		NTPMessage response;
		response.clear();

		int sock = socket(PF_INET, SOCK_DGRAM, IPPROTO_UDP);
		if (INVALID_SOCKET == sock) {
			return;
		}

		do
		{
			WSAEVENT esock = WSA_INVALID_EVENT;
			if (wsaEvnt != NULL && wsaEvnt != WSA_INVALID_EVENT) {
				if ((esock = WSACreateEvent()) != WSA_INVALID_EVENT) {
					WSAEventSelect(sock, esock, FD_READ | FD_WRITE);
				}
			}
			if (SOCKET_ERROR == msg.sendto(sock, addr)) {
				break;
			}
			if (WSA_INVALID_EVENT != esock) {
				HANDLE hWait[] = {
				esock ,
				wsaEvnt,
				};

				if (WSAWaitForMultipleEvents(_countof(hWait), hWait, FALSE, tmout, FALSE) != WSA_WAIT_EVENT_0) {
					break;
				}
				WSASetEvent(wsaEvnt);
			}
			if (SOCKET_ERROR != response.recv(sock)) {
				tmntc.exchange(response.tx.to_time_t());
#ifdef _DEBUG
				auto t = tmntc.load();
				char buf[255] = { 0 };
				ctime_s(buf, 255, &t);
				buf[24] = 0;
				printf("The ntc time is [%s]  svr host:[%s]\n", buf, host);
#endif
			}
			WSACloseEvent(esock);
		} while (false);
		closesocket(sock);
	};

	std::thread* tfs = new std::thread[vecAddr.size()];
	int i = 0;
	for (auto ad : vecAddr) {
		tfs[i++] = std::thread(f, ad);
	}
	i = 0;
	for (auto ad : vecAddr) {
		tfs[i++].join();
	}
	for (auto ad : vecAddr)	{
		free(ad);
	}
}

time_t NTCTime(size_t tmout = DEF_NET_TIMEOUT) {

	WSADATA wsaData;
	if (WSAStartup(MAKEWORD(2, 0), &wsaData)) {
		return 0;
	}

	WSAEVENT e_ok = WSACreateEvent();
	std::thread tfs[sizeof(ntcHosts)];
	int i = 0;
	for (; i < _countof(ntcHosts) && WSAWaitForMultipleEvents(1, &e_ok, false, 0, FALSE) == WSA_WAIT_TIMEOUT; i++)
	{
		/* code */
		tfs[i] = std::thread(NTCHostTime, ntcHosts[i], tmout, e_ok);
		break;
	}

	for (; --i >= 0;)
	{
		/* code */
		tfs[i].join();
	}

	if (e_ok != WSA_INVALID_EVENT) {
		WSACloseEvent(e_ok);
	}
	WSACleanup();

	return tmntc.exchange(0);
}

time_t NTCTimeSingleThread(size_t tmout=DEF_NET_TIMEOUT){
	WSADATA wsaData;
	if (WSAStartup(MAKEWORD(2, 0), &wsaData)) {
		return 0;
	}

	FD_SET set = { 0 };
	FD_ZERO(&set);
	for (auto host:ntcHosts)	{
		std::vector<sockaddr_in*> vecAddr;
		dns_lookup(host, "ntp", vecAddr); /* Helper function defined below. */

		for (auto addr : vecAddr) {
			int sock = socket(PF_INET, SOCK_DGRAM, IPPROTO_UDP);
			if (INVALID_SOCKET == sock) {
				continue;
			}

			FD_SET(sock, &set);

			NTPMessage msg;
			/* Important, if you don't set the version/mode, the server will ignore you. */
			msg.clear();
			msg.version = 3;
			msg.mode = 3 /* client */;

			msg.sendto(sock, addr);
		}
	}

	timeval to = { tmout,0};
	FD_SET setck = set;
	if (select(0, &setck, NULL, NULL, &to) > 0) {
		SOCKET& sock = setck.fd_array[0];
		NTPMessage response;
		response.clear();

		if (SOCKET_ERROR != response.recv(sock)) {
			tmntc.exchange(response.tx.to_time_t());
#ifdef _DEBUG
			auto t = tmntc.load();
			char buf[255] = { 0 };
			ctime_s(buf, 255, &t);
			buf[24] = 0;
			printf("The ntc time is [%s]  svr host:[%s]\n", buf, "");
#endif
		}
	}
	for (int i = 0;i< set.fd_count;i++) {
		SOCKET& sock = set.fd_array[i];
		closesocket(sock);
	}

 	WSACleanup();

	return tmntc.exchange(0);
}