#include <typedef.h>

#if defined(__LINUX__) && defined(__KERNEL__)
# include <linux/kernel.h>
# include <linux/rbtree.h>
#else

#ifndef	RBTREE_H
#define	RBTREE_H

#ifdef _MSC_VER
# ifdef __x86_64__
__declspec(align(8))
# else
__declspec(align(4))
# endif
#endif

struct rb_node {
	uintptr_t rb_parent_color;
#define	RB_RED		0
#define	RB_BLACK	1
	struct rb_node *rb_right;
	struct rb_node *rb_left;
}

#ifndef _MSC_VER
__attribute__((aligned(sizeof(intptr_t))))
#endif
;

struct rb_root {
	struct rb_node *rb_node;
};

#define rb_parent(r)	((struct rb_node *)((r)->rb_parent_color & ~3))
#define rb_color(r)	((r)->rb_parent_color & 1)
#define rb_is_red(r)	(!rb_color(r))
#define rb_is_black(r)	rb_color(r)
#define rb_set_red(r)	do { (r)->rb_parent_color &= ~1; } while (0)
#define rb_set_black(r)	do { (r)->rb_parent_color |= 1; } while (0)

__forceinline__ void rb_set_parent(struct rb_node *rb, struct rb_node *p)
{
	rb->rb_parent_color = (rb->rb_parent_color & 3) | (uintptr_t)p;
}
__forceinline__ void rb_set_color(struct rb_node *rb, int color)
{
	rb->rb_parent_color = (rb->rb_parent_color & ~1) | color;
}

#define RB_ROOT()			{ 0, }

#define RB_EMPTY_ROOT(root)	((root)->rb_node == 0)
#define RB_EMPTY_NODE(node)	(rb_parent(node) == node)
#define RB_CLEAR_NODE(node)	(rb_set_parent(node, node))

extern void rb_insert_color(struct rb_node *, struct rb_root *);
extern void rb_erase(struct rb_node *, struct rb_root *);

/* Find logical next and previous nodes in a tree */
extern struct rb_node *rb_next(const struct rb_node *);
extern struct rb_node *rb_prev(const struct rb_node *);
extern struct rb_node *rb_first(const struct rb_root *);
extern struct rb_node *rb_last(const struct rb_root *);

/* Fast replacement of a single node without remove/rebalance/add/rebalance */
extern void rb_replace_node(struct rb_node *victim, struct rb_node *_new,
	struct rb_root *root);

__forceinline__ void rb_link_node(struct rb_node *node, struct rb_node *parent,
				  struct rb_node **rb_link)
{
	node->rb_parent_color = (uintptr_t)parent;
	node->rb_left = node->rb_right = 0;
	*rb_link = node;
}

#endif /* RBTREE_H */

#endif
