#pragma once

int service_main();

void StartService();

class CService
{
public:
	CService(const TCHAR* name, const TCHAR* group);
	~CService();

	bool IsInstalled();
	bool Install(const TCHAR* path);
	bool UnInstall();

private:
	TCHAR* _name;
	TCHAR* _group;
};

