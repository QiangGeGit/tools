#include "stdafx.h"
#include "Service.h"
#include <stdlib.h>
#include <string.h>


extern TCHAR* svrname;

CService::CService(const TCHAR* name,const TCHAR* group) 
	:_name(_tcsdup(name))
	,_group(_tcsdup(group))
{}

CService::~CService(){
    free(_name);
}

#ifdef _WIN32
#include <windows.h>
#pragma  comment(lib,"Advapi32.lib")

class SCMgr {
public:
    SCMgr(DWORD  dwDesiredAccess) {
        _scm= OpenSCManager(NULL, NULL, dwDesiredAccess);
    }

    ~SCMgr() {
        if (_scm) {
			CloseServiceHandle(_scm);
        }
    }

    operator bool() {
        return !!_scm;
    }

    operator SC_HANDLE& () {
        return _scm;
    }

private:
    SC_HANDLE _scm;
};

bool CService::IsInstalled()
{
    bool bInstalled = false;
    if (!_name) {
        return bInstalled;
    }

	SCMgr mgr(SC_MANAGER_ENUMERATE_SERVICE);
	if (!mgr) {
		return false;
	}

	BYTE* pService = NULL;
	DWORD dwBufNed = 0;
	DWORD dwReturned = 0;
	DWORD dwResumeHanle = 0;
	LSTATUS ls = ::EnumServicesStatusEx(mgr, SC_ENUM_PROCESS_INFO, SERVICE_WIN32, SERVICE_STATE_ALL, pService, 0, &dwBufNed, &dwReturned, &dwResumeHanle, _group);
	pService = (BYTE*)malloc(dwBufNed);
	do
	{
		ls = ::EnumServicesStatusEx(mgr, SC_ENUM_PROCESS_INFO, SERVICE_WIN32, SERVICE_STATE_ALL, pService, dwBufNed, &dwBufNed, &dwReturned, &dwResumeHanle, _group);
		if (!ls) {
			break;
		}
		ENUM_SERVICE_STATUS_PROCESS* pProcess = (ENUM_SERVICE_STATUS_PROCESS*)pService;
		while (dwReturned > 0)
		{
            if (!_tcsicmp(pProcess->lpServiceName, _name)) {
                bInstalled = true;
                break;
            }
			pProcess++;
		}
	} while (false);
	free(pService);

    return bInstalled;
}

bool CService::Install(const TCHAR* path)
{
    if (IsInstalled()) {
        return true;
    }

    SCMgr mgr(SC_MANAGER_CREATE_SERVICE|SC_MANAGER_ENUMERATE_SERVICE);
    if (!mgr) {
        return false;
    }

	BYTE* pService = NULL;
    DWORD dwBufNed = 0;
    DWORD dwReturned = 0;
	DWORD dwResumeHanle = 0;
    LSTATUS ls= ::EnumServicesStatusEx(mgr, SC_ENUM_PROCESS_INFO, SERVICE_WIN32_OWN_PROCESS, SERVICE_STATE_ALL, pService, 0, &dwBufNed, &dwReturned, &dwResumeHanle, _group);
    pService = (BYTE*)malloc(dwBufNed);
    do 
    {
        ls = ::EnumServicesStatusEx(mgr, SC_ENUM_PROCESS_INFO, SERVICE_WIN32_OWN_PROCESS, SERVICE_STATE_ALL, pService, dwBufNed, &dwBufNed, &dwReturned, &dwResumeHanle, _group);
        if (!ls) {
            break;
        }
        ENUM_SERVICE_STATUS_PROCESS* pProcess = (ENUM_SERVICE_STATUS_PROCESS*)pService;
        while (dwReturned>0)
        {
            pProcess++;
        }
    } while (false);
    free(pService);

    auto svrHandle = CreateService(
        mgr,
        _name,
        _name,
        SERVICE_ALL_ACCESS,
        SERVICE_WIN32_OWN_PROCESS,
        SERVICE_AUTO_START,
        SERVICE_ERROR_NORMAL,
        path,
		_group, NULL, _T(""), NULL, NULL);
    CloseServiceHandle(svrHandle);

    return !!svrHandle;
}

bool CService::UnInstall()
{
    SCMgr mgr(SC_MANAGER_ALL_ACCESS);
    if (!mgr) {
        return false;
    }
    auto svrHandle = OpenService(mgr, _name, SERVICE_ALL_ACCESS);
    auto bSuccess = DeleteService(svrHandle);
    CloseServiceHandle(svrHandle);

    return !!bSuccess;
}

static SERVICE_STATUS_HANDLE winsrv_handle = 0;
static SERVICE_STATUS winsrv_stat = { 0 };
__INLINE__ void winsrv_report_stat(unsigned long stat)
{
	static unsigned long checkpt = 1;
	winsrv_stat.dwCurrentState = stat;

	if (stat == SERVICE_RUNNING || stat == SERVICE_STOPPED)
		winsrv_stat.dwCheckPoint = 0;
	else
		winsrv_stat.dwCheckPoint = checkpt++;

	::SetServiceStatus(winsrv_handle, &winsrv_stat);
}
static DWORD WINAPI srvctl_handler_ex(DWORD dwControl, DWORD dwEventType, LPVOID lpEventData, LPVOID lpContext)
{
	DWORD cur_state = winsrv_stat.dwCurrentState;
	switch (dwControl)
	{
	case SERVICE_CONTROL_STOP:
		/* do not support stop */
	{
	}
	case SERVICE_CONTROL_SHUTDOWN:
	{
		winsrv_report_stat(SERVICE_STOP_PENDING);
		printf("[log]SERVICE_CONTROL_SHUTDOWN\n");
		//Scanner::getInstance().control(Scanner::interrupt);
		return NO_ERROR;
	}
	case SERVICE_CONTROL_POWEREVENT:
	{
		return NO_ERROR;
	}
	default:
	{
		break;
	}
	}

	winsrv_report_stat(cur_state);

	return NO_ERROR;
}
static void WINAPI launch_daemon_winsrv(unsigned long argc, LPTSTR argv[])
{
	winsrv_handle = ::RegisterServiceCtrlHandlerEx(svrname, srvctl_handler_ex, NULL);
	if (!winsrv_handle) {
		CLogger::LogError("RegisterServiceCtrlHandlerEx fail with gle[0x%08X]",GetLastError());
		return;
	}

	winsrv_report_stat(SERVICE_RUNNING);
	service_main();
	winsrv_report_stat(SERVICE_STOPPED);
}
void StartService() {
	SERVICE_TABLE_ENTRY service_dispatch[2] = { 0 };
	service_dispatch[0].lpServiceName = _T("");//svrname; //use svrname can crash
	service_dispatch[0].lpServiceProc = launch_daemon_winsrv;
	winsrv_stat.dwServiceType = SERVICE_WIN32_OWN_PROCESS;
	winsrv_stat.dwControlsAccepted = SERVICE_ACCEPT_SHUTDOWN | SERVICE_ACCEPT_SESSIONCHANGE | SERVICE_ACCEPT_POWEREVENT;
	::StartServiceCtrlDispatcher(service_dispatch);
}
#else

#endif