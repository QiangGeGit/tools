#include <string.h>

#ifndef _TYPE_DEFINE_H_
#define _TYPE_DEFINE_H_

#ifndef __MSWIN__
# if defined (_WIN32) || defined (_WIN64)
#  define __MSWIN__ 1
# endif
#endif

#ifndef __LINUX__
# if defined (__linux__)
#  define __LINUX__ 1
# endif
#endif

#ifndef __DARWIN__
# if defined (__APPLE__) || defined (__apple__)
#  define __DARWIN__ 1
# endif
#endif

#ifndef __FREEBSD__
# if defined (__FreeBSD__)
#  define __FREEBSD__
# endif
#endif

#ifndef __BSD__
# if defined (__FreeBSD__) || defined (__DragonFly__) || defined (__NetBSD__) || defined (__APPLE__)
#  define __BSD__
# endif
#endif

#ifndef __KERNEL__
# ifdef KERNEL
#  define __KERNEL__
# endif
#endif

#ifndef __x86_64__
# if defined (__MSWIN__) && defined (_AMD64_)
#  define __x86_64__ 1
# elif defined (__DARWIN__) && defined (__LP64__)
#  define __x86_64__ 1
# endif
#endif

#if defined (__x86_64__) || defined (__mips64) || defined (__aarch64__) || defined (__sw_64) || defined (_LOONGARCH_ARCH_LOONGARCH64)
# define __ARCH_64BIT__ 1
#endif

#if defined(_MSC_VER)
typedef signed char int8_t;
typedef unsigned char uint8_t;
typedef short int16_t;
typedef unsigned short uint16_t;
typedef int int32_t;
typedef unsigned int uint32_t;
typedef __int64 int64_t;
typedef unsigned long long uint64_t;
typedef unsigned long ptr32_t;
typedef unsigned long long ptr64_t;
#elif defined (__LINUX__) && defined (__KERNEL__)
# include <linux/types.h>
#else
# include <stdint.h>
#endif

#ifdef _MSC_VER
# pragma warning (disable: 4201)
#endif

#ifndef _MSC_VER
typedef uint32_t ptr32_t;
typedef uint64_t ptr64_t;
#endif

#ifdef _MSC_VER
# ifdef __ARCH_64BIT__
typedef long long ssize_t;
# else
typedef long ssize_t;
# endif
#endif

#ifdef __MINGW32__
# include <_mingw.h>
#endif

#define __INT8_MIN	(-127-1)
#define __INT16_MIN	(-32767-1)
#define __INT32_MIN	(-2147483647-1)
#define __INT64_MIN	(-9223372036854775807LL-1LL)
#define __INT8_MAX	(+127)
#define __INT16_MAX	(+32767)
#define __INT32_MAX	(+2147483647)
#define __INT64_MAX	(+9223372036854775807LL)
#define __UINT8_MAX	(255)
#define __UINT16_MAX	(65535)
#define __UINT32_MAX	(4294967295U)
#define __UINT64_MAX	(18446744073709551615ULL)
#ifdef __ARCH_64BIT__
# define __INTPTR_MAX	__INT64_MAX
#else
# define __INTPTR_MAX	__INT32_MAX
#endif
#ifdef __ARCH_64BIT__
# define __UINTPTR_MAX	__UINT64_MAX
#else
# define __UINTPTR_MAX	__UINT32_MAX
#endif

/* boolean */
typedef long bool_t;

/* process id */
#ifdef __MSWIN__
typedef uint64_t pid_t;
#endif

/* relative pointer */
typedef int relptr_t;

/* utf-xx string */
typedef char utf8_t;
typedef uint16_t utf16_t;
typedef uint32_t utf32_t;

/* convert native pointer to relptr_t */
#define relptr(_natptr, _base_struct) ((relptr_t)((char *)(_natptr) - (char *)(_base_struct)))
#define mkrelptr(_natptr, _base_struct, _field) \
	do { \
		(_base_struct)->_field = relptr((_natptr), (_base_struct)); \
	} while (0)
/* convert relptr_t to native pointer */
#define natptr(_type, _base_struct, _field) ((_type)((char *)(_base_struct) + (_base_struct)->_field))

#define mkptr32(ptr) (ptr32_t) (uintptr_t) (ptr)
#define mkptr64(ptr) (ptr64_t) (uintptr_t) (ptr)
#define mku32(val) (uint32_t) (uintptr_t) (val)
#define mku64(val) (uint64_t) (uintptr_t) (val)
#define mkpid(val) (pid_t) (uintptr_t) (val)

#define LO8U(u16)  (uint8_t)   (u16)
#define HI8U(u16)  (uint8_t)  ((u16) >> 8)
#define LO16U(u32) (uint16_t)  (u32)
#define HI16U(u32) (uint16_t) ((u32) >> 16)
#define LO32U(u64) (uint32_t)  (u64)
#define HI32U(u64) (uint32_t) ((u64) >> 32)
#define MK16U(lo8,  hi8)  (( uint8_t) ( lo8) | (((uint16_t) ( hi8)) <<  8))
#define MK32U(lo16, hi16) ((uint16_t) (lo16) | (((uint32_t) (hi16)) << 16))
#define MK64U(lo32, hi32) ((uint32_t) (lo32) | (((uint64_t) (hi32)) << 32))

#define TAG32(str) (*(uint32_t *) (str))
#define TAG64(str) (*(uint64_t *) (str))

#define MKVER(major, minor) MK32U(minor, major)
// FIXME: do not use this name!
#if !defined(__LINUX__) || !defined (__KERNEL__)
# define MAJOR(version) HI16U(version)
# define MINOR(version) LO16U(version)
#endif

#if defined(__MSWIN__) || !defined(__KERNEL__)
# include <stddef.h>
# include <sys/types.h>
#endif

#if !defined (__DARWIN__) && (!defined (_MSC_VER) || (_MSC_VER < 1900) || defined (__KERNEL__))
# ifndef __cplusplus
#  ifndef true
#   define true 1L
#  endif
#  ifndef false
#   define false 0L
#  endif
# endif
#else
# include <stdbool.h>
#endif

#if defined (__MSWIN__)
# define LL "I64"
#else
# if defined (__LINUX__) && defined (__ARCH_64BIT__)
#  define LL "l"
# else
#  define LL "ll"
# endif
#endif
#define FMT_LL "%" LL

#ifndef MAX
# define MAX(a, b)  (((a) > (b)) ? (a) : (b))
#endif
#ifndef MIN
# define MIN(a, b)  (((a) < (b)) ? (a) : (b))
#endif

#ifdef _MSC_VER
# define __declspec_align(g) __declspec (align (g))
# define __declspec_thread(x) __declspec (thread) x
#else
# define __declspec_align(g) __attribute__ ((aligned (g)))
# define __declspec_thread(x) __thread x
#endif

/* Size Constants */
#define _1kb	0x00000400
#define _4kb	0x00001000
#define _32kb	0x00008000
#define _64kb	0x00010000
#define _128kb	0x00020000
#define _256kb	0x00040000
#define _512kb	0x00080000
#define _1mb	0x00100000
#define _2mb	0x00200000
#define _4mb	0x00400000
#define _1gb	0x40000000
#define _2gb32	0x80000000U
#define _2gb	0x0000000080000000LL
#define _4gb	0x0000000100000000LL
#define _1tb	0x0000010000000000LL
#define _1pb	0x0004000000000000LL
#define _1eb	0x1000000000000000LL
#define _2eb	0x2000000000000000ULL

/* handle unreferenced variables */
#define __unreferenced(x) x

#ifndef __forceinline__
# if defined(_MSC_VER) || defined(__MINGW32__)
#  define __forceinline__ __forceinline
# else
#  define __forceinline__ static inline
# endif
#endif

#if defined(_MSC_VER)
# define __inline__ __forceinline
# define __INLINE__ __forceinline static
#else
# define __INLINE__ static inline
#endif

#ifndef _MSC_VER
# define __in
# define __out
# define __in_opt
# define __out_opt
# define __out_bcount(x)
#endif

#ifdef __ARCH_64BIT__
# define __CDECL
#else
# ifdef __GNUC__
#  define __CDECL __attribute__((cdecl))
# else
#  define __CDECL __cdecl
# endif
#endif
#define EXPORT_DECL __CDECL

#ifdef __ARCH_64BIT__
# define __asmdecl
#else
# if defined(_MSC_VER)
#  define __asmdecl __cdecl
# else
#  define __asmdecl __attribute__((regparm(0)))
# endif
#endif

#ifdef _MSC_VER
# define ASMDECL(_RetType) _RetType __asmdecl
#else
# define ASMDECL(_RetType) __asmdecl _RetType
#endif

#ifndef glue
#define xglue(x, y) x ## y
#define glue(x, y) xglue(x, y)
#endif

#ifndef likely
# if __GNUC__ < 3
# define __builtin_expect(x, n) (x)
# endif
# ifndef likely
#  define likely(x) __builtin_expect(!!(x), 1)
# endif
# ifndef unlikely
#  define unlikely(x) __builtin_expect(!!(x), 0)
# endif
#endif

#if __GNUC__ >= 4
# define DECL_VISIBILITY_DEFAULT __attribute__ ((visibility ("default")))
# define DECL_VISIBILITY_HIDDEN  __attribute__ ((visibility ("hidden")))
#else
# define DECL_VISIBILITY_DEFAULT
# define DECL_VISIBILITY_HIDDEN
#endif

#ifndef __MSWIN__
# define O_BINARY 0
# define O_TEXT 0
#endif

#define alignup(val, g)	(((val) + (g) - 1) & (~((g) - 1)))
#define aligndown(val, g) ((val) & (~((g) - 1)))

#ifndef offsetof
# define offsetof(type, member) (uintptr_t) &(((type *) 0)->member)
#endif

#if defined(__LINUX__) && defined(__KERNEL__)
# include <linux/kernel.h>
#else
# ifndef ARRAY_SIZE
#  define ARRAY_SIZE(x) (sizeof (x) / sizeof (x[0]))
# endif
# ifndef container_of
#  define container_of(ptr, type, member) \
	((type *) ((char *) (ptr) - offsetof(type, member)))
# endif
#endif

#if !defined (__DARWIN__) || !defined (__KERNEL__)
# ifndef va_copy
#  ifdef __va_copy
#   define va_copy(dst, src) __va_copy(dst, src)
#  else
#   define va_copy(dst, src) ((dst) = (src))
#  endif
# endif
#endif

/**
 * runtime library definations
 * ===========================
 */

#ifdef __MSWIN__
# define strtoll _strtoi64
# define strtoull _strtoui64
#else
# define stricmp strcasecmp
# define strnicmp strncasecmp
#endif

#if defined (_MSC_VER) && (_MSC_VER < 1900)
# define snprintf _snprintf
#endif

/* addtional O_XXX for I/O flags */
#define O_NOCACHE 0x80000000UL

/**
 * common data structure definations
 * =================================
 */

#ifdef __MSWIN__
# define DIR_DELIMIT '\\'
# define DIR_DELIMITS "\\"
#else
# define DIR_DELIMIT '/'
# define DIR_DELIMITS "/"
#endif

extern int kptr_size;

struct ip_addr {
	enum {
		IT_ipv4,
		IT_ipv6,
	} type;
	union{
		uint32_t ipv4;
		struct {
			uint8_t addr[16];
			uint32_t scope_id;
		} ipv6;
	};
};

__INLINE__
bool_t ipaddr_equals(struct ip_addr *ip1, struct ip_addr *ip2)
{
	if (ip1->type != ip2->type)
		return false;
#ifdef __cplusplus
	if (ip1->type == ip_addr::IT_ipv4)
#else
	if (ip1->type == IT_ipv4)
#endif
		return ip1->ipv4 == ip2->ipv4;
	else
		return !memcmp(ip1->ipv6.addr, ip2->ipv6.addr, 16);
}

enum mode_type {
	MT_user,
	MT_kernel,
};

enum address_mode {
	AM_32bit = sizeof(uint32_t),
	AM_64bit = sizeof(uint64_t),
};

#ifdef __ARCH_64BIT__
# define KERNEL_HANDLE_MASK 0xffffffff80000000
#else
# define KERNEL_HANDLE_MASK 0x80000000
#endif

#define __kernel_handle(h) ((intptr_t)(h) < 0)
#define __make_kernel_handle(h) ((void *)((uintptr_t)(h) | KERNEL_HANDLE_MASK))

/* type of kernel callbacks */
enum kernel_callback_type {
	KCT_process = 0UL,
	KCT_thread,
	KCT_image,
	KCT_psnotify_limit,
	KCT_shutdown = KCT_psnotify_limit,
	// TODO:
	KCT_limit,
	KCT_cmapi = KCT_psnotify_limit,
	KCT_miniflt = KCT_psnotify_limit,
};

/* useful length definations */
#define MAX_NAME 1024
#define MAX_SRVNAME 128
#define MAX_FILENAME 128
#define MAX_SHORTNAME 128
#define MAX_PATHNAME 512
#define MAX_DEVNAME 256
#define MAX_CMDLINE 1024
#define MAX_KEYNAME 512
#define MAX_VALNAME 128
#define MAX_VALDATA 256

/**
 * compiler specific settings
 * ==========================
 */

/* disable some warnings for clang */
#ifdef __clang__
# pragma clang diagnostic ignored "-Wmicrosoft"
# pragma clang diagnostic ignored "-Wdeprecated-declarations"
#endif

/* memory barrier */
#if defined (_MSC_VER)
# ifdef __cplusplus
extern "C"
#endif
void _ReadWriteBarrier(void);
# pragma intrinsic(_ReadWriteBarrier)
# define __barrier__() do { _ReadWriteBarrier(); } while (0)
#elif defined (__GNUC__)
# define __barrier__() do { asm volatile("" ::: "memory"); } while (0)
#else
# define __barrier__() do {} while (0)
#endif

/* return address */
#ifndef __cplusplus
# if defined (_MSC_VER)
# ifdef __cplusplus
extern "C"
#endif
void *_ReturnAddress(void);
#  pragma intrinsic (_ReturnAddress)
#  define __builtin_retaddr() _ReturnAddress()
# elif defined (__GNUC__)
#  define __builtin_retaddr() __builtin_return_address(0)
# else
#  error unsupported compiler!
# endif
#endif

/**
 * utility routines
 * ================
 */

__INLINE__
int TOLOWER(int c)
{
	static const uint8_t lower_tab[] = {
		0x00, 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08, 0x09, 0x0a, 0x0b, 0x0c, 0x0d, 0x0e, 0x0f,
		0x10, 0x11, 0x12, 0x13, 0x14, 0x15, 0x16, 0x17, 0x18, 0x19, 0x1a, 0x1b, 0x1c, 0x1d, 0x1e, 0x1f,
		0x20, 0x21, 0x22, 0x23, 0x24, 0x25, 0x26, 0x27, 0x28, 0x29, 0x2a, 0x2b, 0x2c, 0x2d, 0x2e, 0x2f,
		0x30, 0x31, 0x32, 0x33, 0x34, 0x35, 0x36, 0x37, 0x38, 0x39, 0x3a, 0x3b, 0x3c, 0x3d, 0x3e, 0x3f,
		0x40, 0x61, 0x62, 0x63, 0x64, 0x65, 0x66, 0x67, 0x68, 0x69, 0x6a, 0x6b, 0x6c, 0x6d, 0x6e, 0x6f,
		0x70, 0x71, 0x72, 0x73, 0x74, 0x75, 0x76, 0x77, 0x78, 0x79, 0x7a, 0x5b, 0x5c, 0x5d, 0x5e, 0x5f,
		0x60, 0x61, 0x62, 0x63, 0x64, 0x65, 0x66, 0x67, 0x68, 0x69, 0x6a, 0x6b, 0x6c, 0x6d, 0x6e, 0x6f,
		0x70, 0x71, 0x72, 0x73, 0x74, 0x75, 0x76, 0x77, 0x78, 0x79, 0x7a, 0x7b, 0x7c, 0x7d, 0x7e, 0x7f,
		0x80, 0x81, 0x82, 0x83, 0x84, 0x85, 0x86, 0x87, 0x88, 0x89, 0x8a, 0x8b, 0x8c, 0x8d, 0x8e, 0x8f,
		0x90, 0x91, 0x92, 0x93, 0x94, 0x95, 0x96, 0x97, 0x98, 0x99, 0x9a, 0x9b, 0x9c, 0x9d, 0x9e, 0x9f,
		0xa0, 0xa1, 0xa2, 0xa3, 0xa4, 0xa5, 0xa6, 0xa7, 0xa8, 0xa9, 0xaa, 0xab, 0xac, 0xad, 0xae, 0xaf,
		0xb0, 0xb1, 0xb2, 0xb3, 0xb4, 0xb5, 0xb6, 0xb7, 0xb8, 0xb9, 0xba, 0xbb, 0xbc, 0xbd, 0xbe, 0xbf,
		0xc0, 0xc1, 0xc2, 0xc3, 0xc4, 0xc5, 0xc6, 0xc7, 0xc8, 0xc9, 0xca, 0xcb, 0xcc, 0xcd, 0xce, 0xcf,
		0xd0, 0xd1, 0xd2, 0xd3, 0xd4, 0xd5, 0xd6, 0xd7, 0xd8, 0xd9, 0xda, 0xdb, 0xdc, 0xdd, 0xde, 0xdf,
		0xe0, 0xe1, 0xe2, 0xe3, 0xe4, 0xe5, 0xe6, 0xe7, 0xe8, 0xe9, 0xea, 0xeb, 0xec, 0xed, 0xee, 0xef,
		0xf0, 0xf1, 0xf2, 0xf3, 0xf4, 0xf5, 0xf6, 0xf7, 0xf8, 0xf9, 0xfa, 0xfb, 0xfc, 0xfd, 0xfe, 0xff,
	};
	if (c & 0xffffff00)
		return c;
	return lower_tab[c];
}

__INLINE__
int TOUPPER(int c)
{
	static const uint8_t upper_tab[] = {
		0x00, 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08, 0x09, 0x0a, 0x0b, 0x0c, 0x0d, 0x0e, 0x0f,
		0x10, 0x11, 0x12, 0x13, 0x14, 0x15, 0x16, 0x17, 0x18, 0x19, 0x1a, 0x1b, 0x1c, 0x1d, 0x1e, 0x1f,
		0x20, 0x21, 0x22, 0x23, 0x24, 0x25, 0x26, 0x27, 0x28, 0x29, 0x2a, 0x2b, 0x2c, 0x2d, 0x2e, 0x2f,
		0x30, 0x31, 0x32, 0x33, 0x34, 0x35, 0x36, 0x37, 0x38, 0x39, 0x3a, 0x3b, 0x3c, 0x3d, 0x3e, 0x3f,
		0x40, 0x41, 0x42, 0x43, 0x44, 0x45, 0x46, 0x47, 0x48, 0x49, 0x4a, 0x4b, 0x4c, 0x4d, 0x4e, 0x4f,
		0x50, 0x51, 0x52, 0x53, 0x54, 0x55, 0x56, 0x57, 0x58, 0x59, 0x5a, 0x5b, 0x5c, 0x5d, 0x5e, 0x5f,
		0x60, 0x41, 0x42, 0x43, 0x44, 0x45, 0x46, 0x47, 0x48, 0x49, 0x4a, 0x4b, 0x4c, 0x4d, 0x4e, 0x4f,
		0x50, 0x51, 0x52, 0x53, 0x54, 0x55, 0x56, 0x57, 0x58, 0x59, 0x5a, 0x7b, 0x7c, 0x7d, 0x7e, 0x7f,
		0x80, 0x81, 0x82, 0x83, 0x84, 0x85, 0x86, 0x87, 0x88, 0x89, 0x8a, 0x8b, 0x8c, 0x8d, 0x8e, 0x8f,
		0x90, 0x91, 0x92, 0x93, 0x94, 0x95, 0x96, 0x97, 0x98, 0x99, 0x9a, 0x9b, 0x9c, 0x9d, 0x9e, 0x9f,
		0xa0, 0xa1, 0xa2, 0xa3, 0xa4, 0xa5, 0xa6, 0xa7, 0xa8, 0xa9, 0xaa, 0xab, 0xac, 0xad, 0xae, 0xaf,
		0xb0, 0xb1, 0xb2, 0xb3, 0xb4, 0xb5, 0xb6, 0xb7, 0xb8, 0xb9, 0xba, 0xbb, 0xbc, 0xbd, 0xbe, 0xbf,
		0xc0, 0xc1, 0xc2, 0xc3, 0xc4, 0xc5, 0xc6, 0xc7, 0xc8, 0xc9, 0xca, 0xcb, 0xcc, 0xcd, 0xce, 0xcf,
		0xd0, 0xd1, 0xd2, 0xd3, 0xd4, 0xd5, 0xd6, 0xd7, 0xd8, 0xd9, 0xda, 0xdb, 0xdc, 0xdd, 0xde, 0xdf,
		0xe0, 0xe1, 0xe2, 0xe3, 0xe4, 0xe5, 0xe6, 0xe7, 0xe8, 0xe9, 0xea, 0xeb, 0xec, 0xed, 0xee, 0xef,
		0xf0, 0xf1, 0xf2, 0xf3, 0xf4, 0xf5, 0xf6, 0xf7, 0xf8, 0xf9, 0xfa, 0xfb, 0xfc, 0xfd, 0xfe, 0xff,
	};
	if (c & 0xffffff00)
		return c;
	return upper_tab[c];
}

#endif /* _TYPE_DEFINE_H_ */
