#pragma once
#ifndef _MURMUR_H_
#define _MURMUR_H_

uint64_t murmur_hash64(const void* key, int len)
{
	const uint32_t m = 0x5bd1e995;
	const uint32_t seed = 0x19870714;
	const uint32_t* data = (const uint32_t*)key;
	const int r = 24;
	uint32_t h1 = seed ^ len;
	uint32_t h2 = 0;

	while (len >= 8) {
		uint32_t k1, k2;

		k1 = *data++;
		k1 *= m; k1 ^= k1 >> r; k1 *= m;
		h1 *= m; h1 ^= k1;
		len -= 4;

		k2 = *data++;
		k2 *= m; k2 ^= k2 >> r; k2 *= m;
		h2 *= m; h2 ^= k2;
		len -= 4;
	}

	if (len >= 4) {
		uint32_t k1 = *data++;
		k1 *= m; k1 ^= k1 >> r; k1 *= m;
		h1 *= m; h1 ^= k1;
		len -= 4;
	}

	switch (len) {
	case 3: h2 ^= ((uint8_t*)data)[2] << 16;
	case 2: h2 ^= ((uint8_t*)data)[1] << 8;
	case 1: h2 ^= ((uint8_t*)data)[0];
		h2 *= m;
	};

	h1 ^= h2 >> 18; h1 *= m;
	h2 ^= h1 >> 22; h2 *= m;
	h1 ^= h2 >> 17; h1 *= m;
	h2 ^= h1 >> 19; h2 *= m;

	return ((uint64_t)h1 << 32) | h2;
}

uint64_t murmur_hash64_icase(const void* key, int len)
{
	const uint32_t m = 0x5bd1e995;
	const uint32_t seed = 0x19870714;
	const uint32_t* data = (const uint32_t*)key;
	const int r = 24;
	uint32_t h1 = seed ^ len;
	uint32_t h2 = 0;

	while (len >= 8) {
		uint32_t k1, k2;

		k1 = *data++ | 0x20202020;
		k1 *= m; k1 ^= k1 >> r; k1 *= m;
		h1 *= m; h1 ^= k1;
		len -= 4;

		k2 = *data++ | 0x20202020;
		k2 *= m; k2 ^= k2 >> r; k2 *= m;
		h2 *= m; h2 ^= k2;
		len -= 4;
	}

	if (len >= 4) {
		uint32_t k1 = *data++ | 0x20202020;
		k1 *= m; k1 ^= k1 >> r; k1 *= m;
		h1 *= m; h1 ^= k1;
		len -= 4;
	}

	switch (len) {
	case 3: h2 ^= (((uint8_t*)data)[2] | 0x20) << 16;
	case 2: h2 ^= (((uint8_t*)data)[1] | 0x20) << 8;
	case 1: h2 ^= (((uint8_t*)data)[0] | 0x20);
		h2 *= m;
	};

	h1 ^= h2 >> 18; h1 *= m;
	h2 ^= h1 >> 22; h2 *= m;
	h1 ^= h2 >> 17; h1 *= m;
	h2 ^= h1 >> 19; h2 *= m;

	return ((uint64_t)h1 << 32) | h2;
}

#endif /* _MURMUR_H_ */
