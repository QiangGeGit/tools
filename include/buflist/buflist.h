#pragma once

#include <stdlib.h>
#include <container/list.h>

typedef struct {
	int32_t				len;
	struct list_head	lst;
} buflist_t;

typedef struct {
	int32_t				cap;
	int32_t				len;
	struct list_head	next;
} buflist_item_t;

buflist_t*	buflist_alloc();
void		buflist_free(buflist_t *buflist);
void		buflist_clear(buflist_t *buflist);
bool_t		buflist_append(buflist_t *buflist, const void *ibuf, int32_t buflen);
bool_t		buflist_combin(buflist_t *buflist, buflist_t *inbuflist);
int32_t		buflist_copyout(buflist_t *buflist, void *obuf, int32_t buflen);
int32_t		buflist_peek(buflist_t *buflist, void *obuf, int32_t buflen);

bool_t		buflist_memcmp(buflist_t *buflist, int32_t offset, const void *buf, int32_t buflen);
bool_t		buflist_memicmp(buflist_t *buflist, int32_t offset, const void *buf, int32_t buflen);
int32_t		buflist_find(buflist_t *buflist, const void *buf, int32_t buflen);

__INLINE__ buflist_item_t* buflist_item_alloc(int32_t cap)
{
	buflist_item_t *item;
	if ((item = (buflist_item_t *)malloc(cap))) {
		item->len = 0; item->cap = cap;
	}
	return item;
}

