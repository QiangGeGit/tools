#include "buflist.h"

buflist_t* buflist_alloc()
{
	buflist_t *buflist = (buflist_t *)malloc(sizeof(*buflist));
	if (buflist) {
		buflist->len = 0;
		INIT_LIST_HEAD(&buflist->lst);
	}
	return buflist;
}

void buflist_free(buflist_t *buflist)
{
	if (buflist) {
		buflist_clear(buflist);
		free(buflist);
	}
}

void buflist_clear(buflist_t *buflist)
{
	while (!list_empty(&buflist->lst)) {
		buflist_item_t *stub = container_of(buflist->lst.next, buflist_item_t, next);
		list_del(buflist->lst.next);
		free(stub);
	}
	buflist->len = 0;
}

__INLINE__ buflist_item_t* alloc_buflist_item(size_t buflen)
{
	buflist_item_t *stub; size_t stubsz = alignup(sizeof(*stub) + buflen, _4kb);

	if ((stub = (buflist_item_t *)malloc(stubsz))) {
		stub->cap = stubsz - sizeof(*stub);
		stub->len = 0;
	}

	return stub;
}

bool_t buflist_append(buflist_t *buflist, const void *ibuf, int32_t buflen)
{
	buflist_item_t *stub = NULL; int32_t cap;

	if (!list_empty(&buflist->lst)) {
		stub = container_of(buflist->lst.prev, buflist_item_t, next);
		if ((cap = stub->cap - stub->len) == 0) {
			stub = NULL;
		}
	}

	if (stub) {
		if (cap >= buflen) {
			memcpy(((char *)(stub + 1) + stub->len), ibuf, buflen);
			stub->len += buflen;
			buflist->len += buflen;
			return true;
		}
		else {
			memcpy(((char *)(stub + 1) + stub->len), ibuf, cap);
			stub->len += cap; ibuf = (char *)ibuf + cap; buflen -= cap;
			buflist->len += cap;
		}
	}

	if (!(stub = alloc_buflist_item(buflen))) {
		return false;
	}

	memcpy((stub + 1), ibuf, buflen);
	stub->len = buflen;

	list_add_tail(&stub->next, &buflist->lst);
	buflist->len += buflen;

	return true;
}

bool_t buflist_combin(buflist_t *buflist, buflist_t *inbuflist)
{
	buflist->len += inbuflist->len;
	list_splice_tail_init(&inbuflist->lst, &buflist->lst);
	inbuflist->len = 0;
	return true;
}

int32_t buflist_copyout(buflist_t *buflist, void *obuf, int32_t buflen)
{
	buflist_item_t *stub; int32_t outlen = 0;

	if (buflist->len == 0) {
		return 0;
	}

	while (buflen && !list_empty(&buflist->lst)) {
		stub = container_of(buflist->lst.next, buflist_item_t, next);
		if (stub->len <= buflen) {
			memcpy(obuf, (stub + 1), stub->len);
			obuf = (char *)obuf + stub->len; buflen -= stub->len;
			buflist->len -= stub->len; outlen += stub->len;
			list_del(&stub->next); free(stub);
		}
		else {
			memcpy(obuf, (stub + 1), buflen);
			buflist->len -= buflen; outlen += buflen;
			stub->len -= buflen;
			memmove((stub + 1), ((char *)(stub + 1)) + buflen, stub->len);
			break;
		}
	}

	return outlen;
}

int32_t buflist_peek(buflist_t *buflist, void *obuf, int32_t buflen)
{
	buflist_item_t *stub; int32_t outlen = 0;
	struct list_head *p;

	if (buflist->len == 0) {
		return 0;
	}

	list_for_each(p, &buflist->lst) {
		if (buflen <= 0) break;
		stub = container_of(p, buflist_item_t, next);
		if (stub->len <= buflen) {
			memcpy(obuf, (stub + 1), stub->len);
			obuf = (char *)obuf + stub->len; buflen -= stub->len;
			outlen += stub->len;
		}
		else {
			memcpy(obuf, (stub + 1), buflen);
			outlen += buflen;
			break;
		}
	}

	return outlen;
}

__INLINE__ int8_t buflist_get_at(buflist_t *buflist, int32_t offset)
{
	buflist_item_t *stub;
	struct list_head *p;

	list_for_each(p, &buflist->lst) {
		stub = container_of(p, buflist_item_t, next);
		if (offset > stub->len) {
			offset -= stub->len;
		}
		else {
			return ((int8_t *)(stub + 1))[offset];
		}
	}

	return 0;
}

bool_t buflist_memcmp(buflist_t *buflist, int32_t offset, const void *buf, int32_t buflen)
{
	if (buflist->len < offset + buflen) {
		return false;
	}
	for (int32_t i = 0; i < buflen; i++, offset++) {
		if (((int8_t *)(buf))[i] != buflist_get_at(buflist, offset)) {
			return false;
		}
	}
	return true;
}

__INLINE__ int8_t hr_chrlwr(int8_t ch)
{
	return ((ch >= 'A') && (ch <= 'Z')) ? (ch | 0x20) : ch;
}

bool_t buflist_memicmp(buflist_t *buflist, int32_t offset, const void *buf, int32_t buflen)
{
	if (buflist->len < offset + buflen) {
		return false;
	}
	for (int32_t i = 0; i < buflen; i++, offset++) {
		if (hr_chrlwr(((int8_t *)(buf))[i]) != hr_chrlwr(buflist_get_at(buflist, offset))) {
			return false;
		}
	}
	return true;
}

int32_t buflist_find(buflist_t *buflist, const void *buf, int32_t buflen)
{
	for (int32_t offset = 0; offset < buflist->len; offset++) {
		if (buflist_memcmp(buflist, offset, buf, buflen)) {
			return offset;
		}
	}
	return -1;
}