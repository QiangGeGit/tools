#pragma once

#include <string>
#include <codecvt>
#include <locale>

using namespace std;

namespace STRHLP {
	const char to_lower(char c) {
		return c | 'a' - 'A';
	}
	const char to_upper(char c) {
		return c & (~('a' - 'A'));
	}

	static const char* ws = " \t\n\r\f\v";

	// trim from end of string (right)
	inline std::string& rtrim(std::string& s, const char* t = ws)
	{
		s.erase(s.find_last_not_of(t) + 1);
		return s;
	}

	// trim from beginning of string (left)
	inline std::string& ltrim(std::string& s, const char* t = ws)
	{
		s.erase(0, s.find_first_not_of(t));
		return s;
	}

	// trim from both ends of string (right then left)
	inline std::string& trim(std::string& s, const char* t = ws)
	{
		return ltrim(rtrim(s, t), t);
	}

	using convert_t = std::codecvt_utf8<wchar_t>;
	std::wstring_convert<convert_t, wchar_t> strconverter;

	std::string to_string(std::wstring wstr)
	{
		return strconverter.to_bytes(wstr);
	}

	std::wstring to_wstring(std::string str)
	{
		return strconverter.from_bytes(str);
	}
}