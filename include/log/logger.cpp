#include <stdio.h>

#define LOG_FILTER(llc,llf) \
if( llc < llf ) return ;\
va_list va;\
va_start(va, fmt);\
CLogger::GetInstance().Log(fmt, va);\
va_end(va);

#define TIMEFORMAT "[hh:mm:ss.mil]"
#define TIMECHARS (sizeof(TIMEFORMAT)-1)

#ifdef WIN32
#include <windows.h>
#define STDINHANDLE (GetStdHandle(STD_INPUT_HANDLE))
#define STDOUTHANDLE (GetStdHandle(STD_OUTPUT_HANDLE))
#define STDERRORHANDLE (GetStdHandle(STD_ERROR_HANDLE))

BOOL (__stdcall *writeConsole)(
	_In_ HANDLE hConsoleOutput,
	_In_reads_(nNumberOfCharsToWrite) CONST VOID * lpBuffer,
	_In_ DWORD nNumberOfCharsToWrite,
	_Out_opt_ LPDWORD lpNumberOfCharsWritten,
	_Reserved_ LPVOID lpReserved
)=WriteConsoleA;

VOID (__stdcall *dbgout)(
	_In_opt_ LPCSTR lpOutputString
)=OutputDebugStringA;

static LPCSTR curtime() {
	SYSTEMTIME st;
	GetLocalTime(&st);
	static char tm[] = TIMEFORMAT;
	sprintf_s(tm, "[%02d:%02d:%02d.%03d]", st.wHour, st.wMinute, st.wSecond, st.wMilliseconds);
	return tm;
}
#else
#define STDINHANDLE (stdin)
#define STDOUTHANDLE (stdout)
#define STDERRORHANDLE (stderr)

static LPCSTR curtime() {
// 	time_t ltm = 0;
// 	time(&ltm);
// 	struct tm _tm = { 0 };
// 	_localtime64_s(&_tm, &ltm);
// 	static char tm[] = TIMEFORMAT;
// 	sprintf_s(tm, "[%02d:%02d:%02d.%03d]", _tm, st.wMinute, st.wSecond, st.wMilliseconds);
	return NULL;
}
#endif

#include "logger.h"


CLogger::CLogger() :m_msg {0} {
	m_ll = CLogger::LOG_LEVEL::LL_INFO;
	m_prefixlen = TIMECHARS;
}

CLogger::~CLogger()
{
	for (auto v : CLogger::GetInstance().m_viewers) {
		if (v.type == CLogger::LOGVIEWR::ELogViewer::LV_FILE) {
			free((void*)(v.viewer._path));
		}
	}
}

CLogger& CLogger::GetInstance()
{
	// TODO: insert return statement here
	static CLogger logger;
	return logger;
}

void CLogger::Log(LPCSTR fmt, va_list va)
{
	if (!m_mt.try_lock()) {
		return;
	}

	memcpy(m_msg, curtime(), TIMECHARS);
	vsnprintf(m_msg + m_prefixlen, LOG_SIZE-m_prefixlen, fmt, va);
	m_msg[LOG_SIZE-1] = '\0';

	for (auto f:m_viewers){
		switch (f.type)
		{
		case CLogger::LOGVIEWR::ELogViewer::LV_STDOUT: {
			writeConsole(f.viewer._out, m_msg, strlen(m_msg), NULL, NULL);
			break;
		}
		case CLogger::LOGVIEWR::ELogViewer::LV_DBGOUT: {
			dbgout(m_msg);
			break;
		}
		case CLogger::LOGVIEWR::ELogViewer::LV_FILE: {
			Log2File(f.viewer._path, m_msg);
			break;
		}
		default:
			break;
		}
	}

	m_mt.unlock();
}

void CLogger::Log2File(LPCSTR path,LPCSTR log)
{
	if (!path) {
		return;
	}
	FILE* filp;
	if (0 == fopen_s(&filp, path, "a+")) {
		fprintf(filp, "%s\n",
			log);
		fclose(filp);
	}
}

void CLogger::SetViewer(enum CLogger::LOGVIEWR::ELogViewer type)
{
	for (auto v : CLogger::GetInstance().m_viewers) {
		if (v.type == type) {
			return;
		}
	}
	CLogger::LOGVIEWR v = { type };
	if (type == CLogger::LOGVIEWR::LV_STDOUT) {
		v.viewer._out = STDOUTHANDLE;
	}
	m_viewers.push_back(v);
}

void CLogger::SetLogFile(LPCSTR szPath)
{
	if (!szPath) {
		return;
	}
	for (auto v : CLogger::GetInstance().m_viewers) {
		if (v.type == CLogger::LOGVIEWR::ELogViewer::LV_FILE && _stricmp(szPath, v.viewer._path) == 0) {
			return;
		}
	}
	CLogger::LOGVIEWR v;
	v.type = CLogger::LOGVIEWR::ELogViewer::LV_FILE;
	v.viewer._path = (LPCSTR)_strdup(szPath);
	CLogger::GetInstance().m_viewers.push_back(v);
}

void CLogger::Log2Stdout()
{
	CLogger::GetInstance().SetViewer(CLogger::LOGVIEWR::LV_STDOUT);
}

void CLogger::Log2Dbgoutput()
{
	CLogger::GetInstance().SetViewer(CLogger::LOGVIEWR::LV_DBGOUT);
}

void CLogger::SetPrefix(LPCSTR prefix)
{
	if (GetInstance().m_mt.try_lock())
	{
		GetInstance().m_prefixlen = TIMECHARS;
		strcpy_s(GetInstance().m_msg+ GetInstance().m_prefixlen, (LOG_SIZE- GetInstance().m_prefixlen), prefix);
		GetInstance().m_prefixlen = strlen(GetInstance().m_msg + TIMECHARS);
		GetInstance().m_mt.unlock();
	}
}

void CLogger::SetLogLevel(LOG_LEVEL ll)
{
	GetInstance().m_ll = ll;
}

CLogger::LOG_LEVEL CLogger::LogLevel(){
	return LOG_LEVEL(GetInstance().m_ll);
}

void CLogger::LogInfo(LPCSTR fmt, ...)
{
	LOG_FILTER(LL_INFO,CLogger::LogLevel())
}

void CLogger::LogWarning(LPCSTR fmt, ...)
{
	LOG_FILTER(LL_WARNING, CLogger::LogLevel())
}

void CLogger::LogError(LPCSTR fmt, ...)
{
	LOG_FILTER(LL_ERROR, CLogger::LogLevel())
}