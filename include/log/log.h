#pragma once

#include <stdio.h>
#include <stdlib.h>
#include <cstdarg>

#include <windows.h>

#ifndef MAX_PATH
#define MAX_PATH 256
#endif

#ifndef SIZE_VSPF_MSG 
#define  SIZE_VSPF_MSG 2048
#endif

static void ps_logfile(const char* fmt, ...)
{
	static char msg[SIZE_VSPF_MSG] = { 0 };
	va_list va;
	va_start(va, fmt);
	vsnprintf(msg, SIZE_VSPF_MSG, fmt, va);
	va_end(va);

	SYSTEMTIME st;
	GetLocalTime(&st);
	char tm[] = "[hh:mm:ss.mil]";
	sprintf_s(tm, "[%02d:%02d:%02d.%03d]", st.wHour, st.wMinute, st.wSecond, st.wMilliseconds);

#ifdef USE_DBGOUTPUT
	OutputDebugStringA(tm);
	OutputDebugStringA(" ");
	OutputDebugStringA(msg);
	OutputDebugStringA("\r\n");
	return;
#endif
#ifdef USE_CONSOLE
	printf("%s %s\r\n", tm, msg);
	return;
#endif // USE_CONSOLE

	char logfn[MAX_PATH] = { 0 };
	if (!GetModuleFileNameA(GetModuleHandleA(0)
		, logfn
		, (DWORD)(sizeof(logfn)))) {
		return;
	}
	if (!logfn[0])
		return;
	strcat_s(logfn,_countof(logfn), ".log");

	FILE* filp;
	if (0 == fopen_s(&filp, logfn, "a+")) {

		fprintf(filp, "%s %s\n",
			tm,
			msg);
		fclose(filp);
	}
}

#ifdef __cplusplus
class CLog {
public:
	CLog(const char* name,const char* sep="--") :m_name(_strdup(name)),m_sep(_strdup(sep)) {
		ps_logfile("%s%sconstructor", m_name,m_sep);
	};
	~CLog() {
		ps_logfile("%s%sdisConstructor\n", m_name, m_sep);
		free(m_name);
	};

 	void Log(const char* fmt, ...) {
		char msg[SIZE_VSPF_MSG] = { 0 };
		va_list va = NULL;
		va_start(va, fmt);
		vsnprintf(msg, SIZE_VSPF_MSG, fmt, va);
		va_end(va);

		ps_logfile("%s%s%s", m_name,m_sep, msg);
 	}
	
private:
	char* m_name;
	char* m_sep;
};

class CJsonDumps {
public:
	CJsonDumps(json_t* jo) :m_jstr(NULL) {
		m_jstr = json_dumps(jo, JSON_COMPACT);
	}
	~CJsonDumps() {
		json_dumps_free(m_jstr);
	}

	operator const char* () {
		return m_jstr;
	}
private:
	char* m_jstr;
};


#endif