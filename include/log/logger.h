#pragma once
#include <list>
#include <mutex>
#include <cstdarg>

#ifndef LOG_SIZE
#define LOG_SIZE (1024*4)
#endif

#ifndef LPCSTR 
#define  LPCSTR const char*
#endif

class CLogger
{
private:
	CLogger();
	~CLogger();

	static CLogger& GetInstance();
	void Log(LPCSTR fmt, va_list);
	void Log2File(LPCSTR path,LPCSTR log);

public:
	enum LOG_LEVEL {
		LL_INFO,
		LL_WARNING,
		LL_ERROR,
	};

	struct LOGVIEWR {
		enum ELogViewer {
			LV_STDOUT = 0,
			LV_DBGOUT = 1 << 0,
			LV_FILE = 1 << 2,
		}type;
		union 
		{
			FILE* _stream;
			void* _out;
			LPCSTR _path;
		}viewer;
	};

	static void SetLogFile(LPCSTR szPath);
	static void Log2Stdout();
	static void Log2Dbgoutput();

	static void SetPrefix(LPCSTR prefix);
	static void SetLogLevel(LOG_LEVEL ll);
	static LOG_LEVEL LogLevel();

	static void LogInfo(LPCSTR fmt, ...);
	static void LogWarning(LPCSTR fmt, ...);
	static void LogError(LPCSTR fmt, ...);

private:
	void SetViewer(enum CLogger::LOGVIEWR::ELogViewer type);
private:
	std::mutex m_mt;

	LOG_LEVEL m_ll;
	char m_msg[LOG_SIZE];
	size_t m_prefixlen;

	std::list<LOGVIEWR> m_viewers;
};

class CLog {
public:
	CLog(const char* name, const char* sep = "--") :m_name(_strdup(name)), m_sep(_strdup(sep)) {
		CLogger::LogInfo("%s%sconstructor", m_name, m_sep);
	};
	~CLog() {
		CLogger::LogInfo("%s%sdisConstructor\n", m_name, m_sep);
		free(m_name);
	};

	void Log(const char* fmt, ...) {
		char msg[LOG_SIZE] = { 0 };
		va_list va = NULL;
		va_start(va, fmt);
		vsnprintf(msg, LOG_SIZE, fmt, va);
		va_end(va);

		CLogger::LogInfo("%s%s%s", m_name, m_sep, msg);
	}

private:
	char* m_name;
	char* m_sep;
};